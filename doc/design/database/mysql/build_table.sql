SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `kaggle` ;
CREATE SCHEMA IF NOT EXISTS `kaggle` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `kaggle` ;

-- -----------------------------------------------------
-- Table `kaggle`.`s_user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_user` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_user` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_name` VARCHAR(100) NULL ,
  `user_email` VARCHAR(100) NULL ,
  `login_tel` VARCHAR(45) NULL COMMENT 'telephone number' ,
  `password` VARCHAR(100) NULL COMMENT 'stored after encrypt by MD5' ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC) ,
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC) ,
  UNIQUE INDEX `login_tel_UNIQUE` (`login_tel` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`s_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_role` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_role` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `role_name` VARCHAR(100) NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`s_user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_user_role` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_user_role` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `user_id` INT NOT NULL ,
  `role_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_U_ID_idx` (`user_id` ASC) ,
  INDEX `FK_R_ID_idx` (`role_id` ASC) ,
  CONSTRAINT `FK_URU_ID`
    FOREIGN KEY (`user_id` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_URR_ID`
    FOREIGN KEY (`role_id` )
    REFERENCES `kaggle`.`s_role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`s_user_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_user_detail` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_user_detail` (
  `user_id` INT NOT NULL ,
  `display_name` VARCHAR(100) NULL ,
  `birthday` DATE NULL ,
  `occupation` VARCHAR(100) NULL COMMENT '职业' ,
  `organization` VARCHAR(100) NULL COMMENT '组织/公司' ,
  `country` VARCHAR(45) NULL COMMENT '国家' ,
  `address` VARCHAR(200) NULL COMMENT '联系地址' ,
  `university` VARCHAR(45) NULL COMMENT '大学' ,
  `major` VARCHAR(45) NULL COMMENT '专业' ,
  `education` VARCHAR(45) NULL COMMENT '教育程度本科,硕士,博士' ,
  PRIMARY KEY (`user_id`) ,
  CONSTRAINT `FK_UDU_ID`
    FOREIGN KEY (`user_id` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`s_user_authentication`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_user_authentication` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_user_authentication` (
  `user_id` INT NOT NULL ,
  `full_name` VARCHAR(100) NOT NULL COMMENT '实名' ,
  `identity_type` VARCHAR(45) NOT NULL COMMENT '身份证/护照等' ,
  `identity_id` VARCHAR(45) NOT NULL COMMENT '身份证号/护照号等' ,
  `photo` BLOB NULL ,
  PRIMARY KEY (`user_id`) ,
  CONSTRAINT `FK_UAU_ID`
    FOREIGN KEY (`user_id` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_team`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_team` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_team` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `team_name` VARCHAR(45) NOT NULL ,
  `owner` INT NULL COMMENT '团队拥有者' ,
  `create_by` INT NULL COMMENT '创建者' ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_OWNER_ID_idx` (`owner` ASC) ,
  INDEX `FK_CREATE_ID_idx` (`create_by` ASC) ,
  CONSTRAINT `FK_OWNER_ID`
    FOREIGN KEY (`owner` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_TCREATE_ID`
    FOREIGN KEY (`create_by` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_team_member`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_team_member` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_team_member` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `team_id` INT NOT NULL ,
  `user_id` INT NOT NULL ,
  `position` TINYINT NOT NULL DEFAULT 0 COMMENT '团队位置角色\n0:普通成员\n1:队长' ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_USER_ID_idx` (`user_id` ASC) ,
  INDEX `FK_TEAM_ID_idx` (`team_id` ASC) ,
  CONSTRAINT `FK_TMU_ID`
    FOREIGN KEY (`user_id` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_TMT_ID`
    FOREIGN KEY (`team_id` )
    REFERENCES `kaggle`.`b_team` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_competition`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_competition` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_competition` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(100) NOT NULL ,
  `title` VARCHAR(200) NULL ,
  `type` VARCHAR(45) NULL COMMENT '竞赛类型' ,
  `status` VARCHAR(45) NULL COMMENT '状态：Active/Locked\n' ,
  `create_by` INT NULL ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) ,
  INDEX `FK_CREATE_ID_idx` (`create_by` ASC) ,
  CONSTRAINT `FK_CCREATE_ID`
    FOREIGN KEY (`create_by` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_competition_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_competition_detail` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_competition_detail` (
  `competition_id` INT NOT NULL ,
  `description` LONGTEXT NULL COMMENT '竞赛详情描述' ,
  `data` LONGTEXT NULL COMMENT '竞赛数据描述' ,
  `evaluation` LONGTEXT NULL COMMENT '评审描述' ,
  `rules` LONGTEXT NULL COMMENT '规则描述' ,
  `prizes` LONGTEXT NULL COMMENT '奖金描述' ,
  `timeline` LONGTEXT NULL COMMENT '时间安排描述' ,
  PRIMARY KEY (`competition_id`) ,
  CONSTRAINT `FK_CDC_ID`
    FOREIGN KEY (`competition_id` )
    REFERENCES `kaggle`.`b_competition` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_competition_data_file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_competition_data_file` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_competition_data_file` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `competition_id` INT NOT NULL ,
  `file_name` VARCHAR(100) NULL ,
  `file_format` VARCHAR(45) NULL ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  `content` LONGBLOB NULL ,
  `store_path` VARCHAR(200) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_COMPETITION_ID_idx` (`competition_id` ASC) ,
  CONSTRAINT `FK_CDFC_ID`
    FOREIGN KEY (`competition_id` )
    REFERENCES `kaggle`.`b_competition` (`id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_competion_submisson`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_competion_submisson` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_competion_submisson` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `competion_id` INT NOT NULL ,
  `team_id` INT NOT NULL ,
  `create_by` INT NULL ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  `last_update_date` DATETIME NULL ,
  `score` FLOAT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_COMPETITION_ID_idx` (`competion_id` ASC) ,
  INDEX `FK_TEAM_ID_idx` (`team_id` ASC) ,
  INDEX `FK_CREATE_ID_idx` (`create_by` ASC) ,
  CONSTRAINT `FK_CSC_ID`
    FOREIGN KEY (`competion_id` )
    REFERENCES `kaggle`.`b_competition` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_CST_ID`
    FOREIGN KEY (`team_id` )
    REFERENCES `kaggle`.`b_team` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_CSCREATE_ID`
    FOREIGN KEY (`create_by` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`b_competion_submisson_data_file`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`b_competion_submisson_data_file` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`b_competion_submisson_data_file` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `competition_submission_id` INT NULL ,
  `version_number` FLOAT NOT NULL DEFAULT 0.0 ,
  `version_tag` VARCHAR(45) NULL ,
  `create_by` INT NULL ,
  `create_date` DATETIME NOT NULL DEFAULT NOW() ,
  `file_name` VARCHAR(100) NULL ,
  `file_format` VARCHAR(45) NULL ,
  `content` LONGBLOB NULL ,
  `store_path` VARCHAR(200) NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_CS_ID_idx` (`competition_submission_id` ASC) ,
  INDEX `FK_CREATE_ID_idx` (`create_by` ASC) ,
  CONSTRAINT `FK_CSDFCS_ID`
    FOREIGN KEY (`competition_submission_id` )
    REFERENCES `kaggle`.`b_competion_submisson` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `FK_CSDFCREATE_ID`
    FOREIGN KEY (`create_by` )
    REFERENCES `kaggle`.`s_user` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `kaggle`.`s_permission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `kaggle`.`s_permission` ;

CREATE  TABLE IF NOT EXISTS `kaggle`.`s_permission` (
  `id` INT NOT NULL ,
  `permission_name` VARCHAR(45) NOT NULL ,
  `role_id` INT NOT NULL ,
  `description` TEXT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `FK_ROLE_ID_idx` (`role_id` ASC) ,
  CONSTRAINT `FK_PR_ID`
    FOREIGN KEY (`role_id` )
    REFERENCES `kaggle`.`s_role` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `kaggle` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
